import {GbRoll} from "../roll.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class GbActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  async _onCreate(data, options, user) {
    await super._onCreate(data, options, user)
    if (this.type === 'character') {
      await this.prototypeToken.update({_id: this.id, id: this.id, vision: true, actorLink: true, disposition: 1});
    }
  }

  prepareData() {
    super.prepareData();

    const actorData = this.system;
    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    //const data = actorData.data;
  }

  prepareDerivedData() {
    super.prepareDerivedData();
    //Calculate muscle encumbrance from items
    let enc = 0;
    for (let item of this.items) {
      enc += item.system.muscle;
    }
    this.system.muscle = enc;
    for (let trait in this.system.traits) {
      if (this.system.traits[trait].attached !== false) {
        this.system.traits[trait].attached.value = (+this.system.traits[trait].current) + 3;
      }
    }
  }

  /**
   * Stub for handling macro rolls
   * @param trait  The trait being rolled
   */
  async onRollMacro(type, trait) {
    const rollData = {};
    if(type === "trait") {
      rollData.roll = this.system.traits[trait].current;
      rollData.label = game.i18n.localize(this.system.traits[trait].label);
    } else if(type === "talent") {
      rollData.roll = this.system.traits[trait].attached.value;
      rollData.label = game.i18n.localize(this.system.traits[trait].label) + " " +
          game.i18n.localize(this.system.traits[trait].attached.label);
    }
    if (rollData.roll === 0) {
      ui.notifications.warn(game.i18n.localize("GB.SCORE_0"));
      return;
    };
    rollData.brownie_points = 0;
    rollData.bonus = 0;
    rollData.penalty = 0;
    rollData.actor = this;
    rollData.bpcostcolor = 'black';
    await GbRoll.rollDialog(rollData);
  }
}
/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class GbItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();
    // Get the Item's data
  }
}

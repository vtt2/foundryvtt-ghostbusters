export class GbRollDialog extends Dialog {
    constructor(rollData, data, options) {
        super(data, options);
        this.rollData = rollData;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('.bpup').click(async () => {
            if ((+this.rollData.brownie_points) >= this.rollData.actor.data.data.brownie_points.value) {
                ui.notifications.warn(game.i18n.localize("GB.NOT_ENOUGH_BROWNIE_POINTS"));
            } else {
                this.rollData.brownie_points++;
                await this.updateDialog();
            }
        });

        html.find('.bpdown').click(async () => {
            if (this.rollData.brownie_points > 0) {
                this.rollData.brownie_points--;
            }
            await this.updateDialog();
        });

        html.find('.bonus').change(async (ev) => {
            this.rollData.bonus = ev.target.value;
        })

        html.find('.penalty').change(async (ev) => {
            this.rollData.penalty = ev.target.value;
        })
    }

    async updateDialog() {
        this.rollData.brownie_points > this.rollData.actor.data.data.brownie_points.value ? this.rollData.bpcostcolor = "red" :
            this.rollData.bpcostcolor = "black";
        const rollTemplate = "systems/gb/templates/roll.html";
        this.data.content = await renderTemplate(rollTemplate, this.rollData);
        this.render();
    }
}

export class GbRoll {
    activateListeners(html) {
        super.activateListeners(html);
    }

    static async rollDialog(rollData) {
        const template = "systems/gb/templates/roll.html";
        const html = await renderTemplate(template, rollData);
        await new GbRollDialog(rollData, {
            title: game.i18n.localize('GB.ROLL'),
            content: html,
            buttons: {
                submit: {
                    label: game.i18n.localize('GB.ROLL'),
                    callback: () => GbRoll.rollAction(rollData)
                }
            }
        }).render(true);
    }

    static async rollAction(rollData) {
        let rollString = '';
        rollData.roll = (+rollData.roll) -1 - (+rollData.penalty);
        if (rollData.roll > 0) {
            rollString += rollData.roll + "d6[Base]+";
        }
        if (rollData.brownie_points > 0) {
            rollString += rollData.brownie_points + "d6[Brownie Points]+";
            const update = {};
            update.data = {};
            update.id = rollData.actor.id;
            update._id = rollData.actor.id;
            update.data.brownie_points = {};
            update.data.brownie_points.value = (+rollData.actor.data.data.brownie_points.value) - (+rollData.brownie_points);
            await rollData.actor.update(update);
        }

        if (rollData.bonus > 0) {
            rollString += rollData.bonus + "d6[Bonus]+";
        }

        rollString += "1dg[Ghost]"

        const roll = await new Roll(rollString).evaluate({"async": true});
        let label = rollData.label ? `${game.i18n.localize('GB.ROLLING')} ${rollData.label}` : '';
        let ghostResult = roll.terms.find(d => d.options.flavor === "Ghost").total;
        if (ghostResult === 0) {
            label = label + "<span style=\"color:red;\">";
            label = label + " " + game.i18n.localize("GB.GHOST_DIE").toUpperCase() + "!";
            label = label + "</span>";
        }

        const chatMessage = await roll.toMessage({
                speaker: ChatMessage.getSpeaker({actor: rollData.actor}),
                flavor: label
        });
    }
}

export class GhostDie extends DiceTerm {
    constructor(termData) {
        super(termData);
        this.faces = 6;
    }
    static DENOMINATION = "g";
    static flavor = "Ghost";

    roll({minimize=false, maximize=false}={}) {
        const roll = {result: undefined, active: true};
        if (minimize) roll.result = Math.min(1, this.faces);
        else if (maximize) roll.result = this.faces;
        else roll.result = Math.ceil(CONFIG.Dice.randomUniform() * this.faces);
        if (roll.result === 6) roll.result = 0;
        this.results.push(roll);
        return roll;
    }
}

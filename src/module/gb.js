// Import Modules
import { GbActor } from "./actor/actor.js";
import { GbActorSheet } from "./actor/actor-sheet.js";
import { GbItem } from "./item/item.js";
import { GbItemSheet } from "./item/item-sheet.js";
import {GbRoll, GhostDie} from "./roll.js";
import GB from "./config.js";

Hooks.once('init', async function() {

  game.Gb = {
    GbActor,
    GbItem,
    GbRoll,
    diceTerms: [GhostDie],
  };

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "@traits.moves.current + (@traits.cool.current/10)",
    decimals: 2
  };
  // Define custom Document classes
  CONFIG.Actor.documentClass = GbActor;
  CONFIG.Item.documentClass = GbItem;

  CONFIG.Dice.terms["g"] = GhostDie;

  game.settings.register("gb", "ghost_die_face", {
    name: game.i18n.localize('GB.CONFIG_GHOST_DIE_FACE'),
    hint: game.i18n.localize("GB.CONFIG_GHOST_DIE_FACE_DESCRIPTION"),
    scope: "world",
    config: true,
    default: "systems/gb/icons/spectre.png",
    type: String,
    filePicker: "image"
  })

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("Gb", GbActorSheet, { makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("Gb", GbItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('toUpperCase', function(str) {
    return str.toUpperCase();
  });

  Handlebars.registerHelper('getMed', function() {
    return GB.medical;
  })

  Handlebars.registerHelper('getMedEffect', function(level) {
    return GB.medical[level].effect;
  })

  Handlebars.registerHelper('getMuscleEffect', function(muscles, muscle) {

    let ratio =  (+muscle)/(+muscles);
    if (ratio >= 3) {
      ratio = 4;
    } else if (ratio >= 2) {
      ratio = 3;
    } else if (ratio > 1) {
      ratio = 2;
    } else {
      ratio = 1;
    }
    return GB.muscle_effect[ratio].label;
  })

  await loadHandlebarTemplates();

});

async function loadHandlebarTemplates() {
  const templatePaths = [
    "systems/gb/templates/actor/data.html"
  ]
  await loadTemplates(templatePaths);
}

Hooks.on('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({id: 'gb',name: "Ghostbusters"})
    dice3d.addDicePreset({
      type: "dg",
      labels: [game.settings.get('gb','ghost_die_face'),"1","2","3","4","5"],
      colorset: "white",
      values: {min:0,max:5},
      system: "gb"
    }, "d6")
})

Hooks.on('diceSoNiceRollStart', (messageId, context) => {
  const roll = context.roll;
  let die;
  let len = roll.dice.length;
  // Customize colors for Dice So Nice
  for (die = 0; die < len; die++) {
    switch (roll.dice[die].options.flavor) {
      case "Ghost":
        roll.dice[die].options.colorset = "white";
        break;
      case "Brownie Points":
        roll.dice[die].options.colorset = "black";
        break;
      default:
        break;
    }
  }
})
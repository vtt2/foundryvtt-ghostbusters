const GB = {};

GB.medical = {
    "6": {
        "label": "GB.UNHURT",
        "effect": "GB.UNHURT_EFFECT"
    },
    "5": {
        "label": "GB.BANGED_UP_SOME",
        "effect": "GB.BANGED_UP_SOME_EFFECT"
    },
    "4": {
        "label": "GB.HURT",
        "effect": "GB.HURT_EFFECT"
    },
    "3": {
        "label": "GB.REALLY_HURT",
        "effect": "GB.REALLY_HURT_EFFECT"
    },
    "2": {
        "label": "GB.TRASHED",
        "effect": "GB.TRASHED_EFFECT"
    },
    "1": {
        "label": "GB.BASKET_CASE",
        "effect": "GB.BASKET_CASE_EFFECT"
    },
    "0": {
        "label": "GB.NEARLY_DEAD",
        "effect": "GB.NEARLY_DEAD_EFFECT"
    }
}

GB.locations = [
    "GB.HEAD",
    "GB.BACK",
    "GB.BELT",
    "GB.HANDS"
]

GB.muscle_effect = {
    "1": {
        "label": "GB.MUSCLE_1"
    },
    "2": {
        "label": "GB.MUSCLE_2"
    },
    "3": {
        "label": "GB.MUSCLE_3"
    },
    "4": {
        "label": "GB.MUSCLE_4"
    }
}

export default GB;
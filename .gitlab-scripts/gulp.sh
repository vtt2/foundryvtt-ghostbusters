#!/bin/bash

set -euo pipefail
set -x 

npm install --global gulp-cli@2.3.0
npm init --yes
npm install --save-dev sass gulp@4.0.2 gulp-autoprefixer@8.0.0 gulp-sass through2 js-yaml nedb merge-stream gulp-clean gulp-sourcemaps
gulp --version
gulp build

